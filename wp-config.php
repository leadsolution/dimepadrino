<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dimepadrino');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'gE2LIx88VE');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'dfqM5vv;8jT_hJQOyj6_$<5]lc-yJeGJ2%]ECpKm3RNV|Bvtc*`/@[m&/}Ji<K2Z');
define('SECURE_AUTH_KEY',  '9jfi9M-6-SB6,af=[GoyAg&2dm[Oqe;{Brm6ef.TJ)w3_JAq|#wP%0o_LKIPtph,');
define('LOGGED_IN_KEY',    '&<F;PMo$D))889&zUw|b :^v><91CSuD2`H~HbEv1[wBF(/ML~RAoR;KI3H@{7,_');
define('NONCE_KEY',        '1A-cdH($^8dj!t=|^v@wI?NS?m130-Z^h;_7~qUj}lF2R1LX]}ElZmk7K|ERv}EA');
define('AUTH_SALT',        '-Q;C#u8mxkiqsu}}JB AO)V}$w`yC%CazO0>ziI?63pkPft)]sgFzI-Lq!O0Hd@;');
define('SECURE_AUTH_SALT', 'agGD%#^&-5hpk(n0Y92f>MMmY .yx,Ai5aA njP[4U.ib3nyU[5N8>oVn:eompd5');
define('LOGGED_IN_SALT',   'z>in=V-Mz`Smwu.yFd}%n~hK`QqVaT/GFxaGl.3c_kkvi&lM0ZVE`9;@oSB!be`a');
define('NONCE_SALT',       '&IDvc_RtXR,:w^<~8XPo6WV*(%B9/y$1eeg!l-5ZPJ#Nyl6iX{qG=s|??|6l`CT=');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
