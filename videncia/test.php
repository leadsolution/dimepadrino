<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Chat Videncia</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta charset="UTF-8">

  <link rel='stylesheet prefetch' href='http://cdn.materialdesignicons.com/1.1.70/css/materialdesignicons.min.css'>
  <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Roboto:300'>
  <link rel="stylesheet" href="css/style.css">

  <link rel="stylesheet" type="text/css" href="dist/jquery.convform.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/demo.css">
</head>
<body>
   <style id="dynamic-styles"></style>

<div id="hangout">

  <div id="head" class="style-bg">
   <img src="image/videncia1.jpg" style="margin: 0px;">
    <h1>Vidente Online</h1><i class="mdi mdi-chevron-down"></i>
  </div>
  <div id="content">
   <div class="list-chat">
    <ul class="chat">
     <div id="demo">
	     <div class="vertical-align">
	      <div class="container">
	       <div class="row">
	        <div id="hangout">
	         <div class="card no-border">
	          <div id="chat" class="conv-form-wrapper">
	           <form action="curl.php" method="GET" class="hidden">

             <input type="text" name="name" conv-question="Hola soy la vidente Quisiera saber tu Nombre">

             <input type="text" name="lastname" conv-question="{name} Puedes decirme tu apellido">

	            <input type="text" conv-question="Gracias, {name} {lastname}! hablemos un poco para poder hacer una prediccion! para ello necesito varios datos personales." no-answer="true">

	            <input type="date" name="birthday" conv-question="Puedes darme tu dia de nacimiento en dos digitos" pattern="([0-3]{2})">

	            <input type="date" name="birthmonth" conv-question="Interesante dia me puedes dar el mes." pattern="(\d{2})">

	            <input type="date" name="birthyear" conv-question="Wao, con el año puedo completar una prediccion" pattern="(\d{4})">

	            <input type="text" conv-question="Naciste, el {birthday}/{birthmonth}/{birthyear} digo prediccion." no-answer="true">

	            <input conv-question="Para empezar con tu Prediccion debes permitirme tu correo electronico" pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" id="email" type="email" name="email" required placeholder="Cual es tu correo?">

             <input conv-question="Gracias, me puedes permitir tu numero de telefono" pattern="[0-9]" type="text" data-minlength="10" id="senha" name="phone" required placeholder="Telefono">

             <input conv-question="Gracias tienes algun codigo postal" pattern="^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$" type="text" data-minlength="10" id="senha" name="postal" required placeholder="Telefono">

             <select class="pull-right" name="meta_sexo" conv-question="Eres hombre o mujer?">
	             <option value="M">Mujer</option>
	             <option value="H">Hombre</option>
	            </select>

             <select conv-question="Deseas saber?">
              <option value="Si" callback="google">Futuro</option>
              <option value="nada" callback="bing">Amor</option>
								     </select>

								     <select conv-question="Gracias por Visitar Nuestra VIDENCIA? te gusto." id="">
	             <option value="">Si</option>
	             <option value="">No</option>
	            </select>
	           </form>
	          </div>
	         </div>
	        </div>
	       </div>
	      </div>
	     </div>
	    </div>
    </ul>
   </div>
  </div>
</div>

  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src="js/index.js"></script>
  <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
  <script type="text/javascript" src="dist/autosize.min.js"></script>
  <script type="text/javascript" src="dist/jquery.convform.js"></script>
  <script>
	    function menor() {
		    alert('No debes estar en esta chat !!');
	    }
  </script>

</body>
</html>
