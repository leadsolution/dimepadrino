<?php

$url                = "http://leadsolution.afiliasolution.com/leads";
$data               = array();
$data['source']     = 'ORVDORG';//source para natexo
$data['format']     = 'json';
$data['id_country'] = 1;//pais españa
// Variables Videndia
$data['firstname']        = $_GET['name'];
$data['lastname']         = $_GET['lastname'];
$data['meta_Nacimiento']  = $_GET['birthday'].'/'.$_GET['birthmonth'].'/'.$_GET['birthyear'];
$data['email']            = $_GET['email'];
$data['mobile_phone']     = $_GET['phone'];
$data['metal_CP']         = $_GET['postal'];
$data['meta_sexo']        = $_GET['meta_sexo'];
$data['meta_desea_saber'] = $_GET['interes'];

$ch = curl_init();

$opts = array(
	CURLOPT_URL            => $url,
	CURLOPT_POST           => true,
	CURLOPT_POSTFIELDS     => $data,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_REFERER        => $_SERVER['HTTP_REFERER'],
);

curl_setopt_array($ch, $opts);

$response = curl_exec($ch);

$response = json_decode($response);

header('Location: http://dimepadrino.com/videncia/');